#include "grid/multigrid.h"
#include "poisson.h"
#include "utils.h"
#define pi 3.1415926
//2.1 concentration source and flux, and some obvious variables
scalar c[],s[],flux[];

double dx,dy;
//2.2 the exact solution without mask
double cexact(double x, double y)
{ double ce;
ce =0.;
//ce = (bi*y+1)/(bi*L0+1);
for (double l = 1 ; l <= 200; l += 1){
for (double m= 1 ; m<= 200; m += 1){
ce += -16*sin((2*l-1)*pi*x)*sin((2*m-1)*pi*y)/(((2*l-1)*(2*l-1)+(2*m-1)*(2*m-1))*pi*pi*pi*pi*(2*l-1)*(2*m-1));
}
}
return ce;
}
//2.3 BC dirichlet 1
c[top]= dirichlet(0);
c[bottom] = dirichlet(0);
c[right] = dirichlet(0);
c[left]= dirichlet(0);

int main() {

//2.4 Parameters: the size of the domain L0 . 0<x<L0 0<y<L0
L0 = 1.;
X0 = 0.;
Y0 = 0.;
int N = 21;
dx = L0/N;
dy = L0/N;
init_grid (N);

//2.5 Solve Laplace:
//------------------
// 2.5.1 Initialisation of the solution of laplace equation with poisson ∇2c=s with a source term s=1
foreach()
c[] = 0.;
foreach()
s[]= 1.;
boundary ({c, s});
poisson (c,s);

// 2.5.2 Computation of the flux
foreach()
flux[] = ( c[0,0] - c[0,-1] )/dx;
boundary ({flux});
//2.6 Save the results
//---------------------
//2.6.1 analytical solution
FILE * fpx = fopen("Fce.txt", "w");
for (double x = 0 ; x <=1; x += dx){
for (double y = 0; y <= 1; y += dy){
fprintf (fpx, "%g %g %g \n",x, y, cexact (x,y));}
fprintf (fpx,"\n");}
fclose(fpx);
//2.6.2 numerical solution
FILE * fpc = fopen("Fcs.txt", "w");
for (double x = 0 ; x <1-dx; x += dx){
for (double y = 0; y < 1-dy; y += dy){
fprintf (fpc, "%g %g %g \n",x, y,interpolate (c, x, y));}
fprintf (fpc,"\n");}
fclose(fpc);
//2.6.3 Error
FILE * fpe = fopen("Fcerror.txt", "w");
static double abscemax=0., cemax=0., abscsmax=0., csmax=0.;
for (double x = 0 ; x <=1; x += dx){
for (double y = 0; y <= 1; y += dy){
cemax = fabs(cexact (x,y));
if(cemax > abscemax) abscemax = cemax;
csmax = fabs( interpolate (c, x, y));
if(csmax > abscsmax) abscsmax = csmax;
}}
fprintf (fpe, "%d %g %g \n",N,dx ,fabs((abscemax-abscsmax)/abscemax));
fclose(fpe);
fprintf(stdout,"Err = ");
fprintf(stdout," %g\n", fabs((abscemax-abscsmax)/abscemax));
fprintf(stdout," end\n");
}
