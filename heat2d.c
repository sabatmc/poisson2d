#include "grid/multigrid.h"
#include "run.h"
#include "poisson.h"

//Temperature and flux, and some obvious variables
scalar T[],flux[];
double Bi,tmax,dy;

//The generic time loop needs a timestep. We will store the statistics on the diffusion solvers in mgdpoi.
mgstats mgpoi;

//the exact solution without mask
double Texact(double y)
{ double Te;
	   Te = (Bi*y+1)/(Bi*L0+1);	 	 	
	    return Te;	
}
//Boundary conditions: a given temperature at the top, a mixed convection at the bottom
T[top]    =  dirichlet(1);
T[bottom] =  T[]*(2.-Bi*dy)/(2.+Bi*dy) ;
T[right]  =  neumann(0);
T[left]   =  neumann(0);

int main() {
	L0 = 10.;
	X0 = -L0/2;
	Y0 = 0;
	N =  32;
	dy = L0/N;
	tmax = 500;
	Bi = .5;
	run();
}
//Initial conditions
event init (i = 0) {
	   foreach()
		       T[] = 0.;
	     boundary ({T});
}
//integration
event integration (i++) {
//prepare Poisson solver with zero source
scalar source[]; 
  foreach() {
	      source[] = 0;
	        }   
    mgpoi = poisson (T,source);
    foreach()
	        flux[] =  - ( T[0,0] - T[0,-1] )/dy;
      boundary ({flux});       
}
//outputs:print data, saves along the center
event printdata ( i++) {
	FILE *  fpx = fopen("T.txt", "w");
	      
	for (double y = 0 ; y <= L0; y += dy){
	   fprintf (fpx, "%g %g %g \n",y, interpolate (T, 0, y) , interpolate (flux, 0, y));}
	               
	fclose(fpx); 	    
}
//At the end of the simulation, we create snapshot images of the field, in PNG format.
event pictures (i++) {
	  output_ppm (T, file = "T.png", min=0, max = 1,  spread = 2, n= 128, linear = true);
}
